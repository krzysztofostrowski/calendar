<?php
/**
 * Created by PhpStorm.
 * User: krzysztofostrowski
 * Date: 26.03.2014
 * Time: 21:10
 */

namespace KO\Calendar\CalendarBundle\Controller;

use KO\Calendar\CalendarBundle\Logic\Calendar;
use KO\Calendar\CalendarBundle\Logic\Participant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{

    /**
     * @Route("/calendar")
     * @Template()
     */
    public function indexAction()
    {
        $calendar = new Calendar();
        $calendar->setNumberOfSlotsRequiredForMeeting(21);
        $calendar->setSlotDurationTime(30);
        $calendar->setNumberOfTimeSlotsToFind(2);

        $participant = new Participant('pierwszy');
        $participant->setStartOfWorkingHours('08:40');
        $participant->setEndOfWorkingHours('16:00');
        $calendar->addParticipant($participant);

        $participant = new Participant('drugi');
        $participant->setStartOfWorkingHours('10:00');
        $participant->setEndOfWorkingHours('18:00');
        $calendar->addParticipant($participant);

        $participant = new Participant('trzeci');
        $participant->setStartOfWorkingHours('13:00');
        $participant->setEndOfWorkingHours('20:00');
        $calendar->addParticipant($participant);

        var_dump($calendar->arrangeMeeting());

        return array('name' => '');
    }
}

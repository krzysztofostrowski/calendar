<?php
/**
 * Created by PhpStorm.
 * User: krzysztofostrowski
 * Date: 26.03.2014
 * Time: 21:16
 */

namespace KO\Calendar\CalendarBundle\Logic;

class Calendar
{

    private $slotMatrix;
    private $slotDurationTime = 60;
    private $availableSlotDurationTimes = array(15, 30, 60);
    private $numberOfInvitedParticipants = 0;
    private $numberOfSlotsRequiredForMeeting = 1;
    private $invitedParticipants = array();
    private $numberOfTimeSlotsToFind = 1;

    /**
     * @param int $numberOfTimeSlotsToFind
     */
    public function setNumberOfTimeSlotsToFind($numberOfTimeSlotsToFind)
    {
        if (!is_int($numberOfTimeSlotsToFind) or $numberOfTimeSlotsToFind < 1) {
            throw new \Exception('Number of time slot to find should be number greater than 1');
        }
        $this->numberOfTimeSlotsToFind = $numberOfTimeSlotsToFind;
    }

    /**
     * @return int
     */
    public function getNumberOfTimeSlotsToFind()
    {
        return $this->numberOfTimeSlotsToFind;
    }

    /**
     * @param int $slotDurationTime
     */
    public function setSlotDurationTime($slotDurationTime)
    {
        if (!is_int($slotDurationTime) or !in_array($slotDurationTime, $this->availableSlotDurationTimes)) {
            throw new \Exception('Slot duration time should be ' . implode(' or ', $this->availableSlotDurationTimes) . ' minutes');
        }
        $this->slotDurationTime = $slotDurationTime;
    }

    /**
     * @return int
     */
    public function getSlotDurationTime()
    {
        return $this->slotDurationTime;
    }

    /**
     * @return mixed
     */
    public function getNumberOfInvitedParticipants()
    {
        return $this->numberOfInvitedParticipants;
    }

    /**
     * @param int $numberOfSlots
     */
    public function setNumberOfSlotsRequiredForMeeting($numberOfSlots)
    {
        if (!is_int($numberOfSlots) or $numberOfSlots < 1 or $numberOfSlots > $this->amountOfSlotsPerDay()) {
            throw new \Exception('Number of slots required for meeting should be between 1 and ' . $this->amountOfSlotsPerDay());
        }

        $this->numberOfSlotsRequiredForMeeting = $numberOfSlots;
    }

    private function amountOfSlotsPerDay()
    {
        return 24 * 60 / $this->getSlotDurationTime();
    }

    /**
     * @return mixed
     */
    public function getNumberOfSlotsRequiredForMeeting()
    {
        return $this->numberOfSlotsRequiredForMeeting;
    }

    public function addParticipant(Participant $participant)
    {
        $this->invitedParticipants[] = $participant;
        $this->prepareFreeSlotsRepresentationForParticipant($participant);
        $this->updateNumberOfParticipants();
    }

    private function prepareFreeSlotsRepresentationForParticipant(Participant $participant)
    {
        $slots = array_fill(0, $this->amountOfSlotsPerDay(), 0);

        $start = $this->convertTimeToSlot($participant->getStartOfWorkingHours());
        $end   = $this->convertTimeToSlot($participant->getEndOfWorkingHours(), 'end_slot');

        $slots = array_replace($slots, array_fill_keys(range($start, $end), (1 << $this->getNumberOfInvitedParticipants())));

        $this->slotMatrix[] = $slots;
    }

    private function sumFreeParticipantsPerSlot()
    {
        $this->slotMatrix = array_map('array_sum', $this->slotMatrix);
    }

    private function cutOffSlotsWithoutAnyFreeParticipant()
    {
        $this->slotMatrix = array_diff($this->slotMatrix, array(0));
    }

    private function getSlotsWithAllInvited()
    {
        return array_intersect($this->slotMatrix, array($this->sumAllInvited()));
    }

    private function sumAllInvited()
    {
        return pow(2, $this->numberOfInvitedParticipants) - 1;
    }

    public function arrangeMeeting()
    {
        $this->prepare();

        $listOfMeetingTimesForAll = $this->findMeetingStartsForAllInvited();
        $listOfMeetingTimesForAll = $this->limitTimeSlots($listOfMeetingTimesForAll);

        array_walk($listOfMeetingTimesForAll, array($this, 'convertSlotToTime'));

        if ( count($listOfMeetingTimesForAll)) {
            $return['list_of_slots'] = array_values($listOfMeetingTimesForAll);
        } else {
            $return['list_of_slots'] = 'Sorry, its not possible to arrange meeting with everyone in selected time-frame';
            $this->analyzeParticipantsCount();
        }

        return $return;
    }

    private function limitTimeSlots($listOfMeetingTimeSlots)
    {
        return array_slice($listOfMeetingTimeSlots, 0, $this->getNumberOfTimeSlotsToFind(), true);
    }

    private function prepare()
    {
        $this->transposeSlotMatrix();
        $this->sumFreeParticipantsPerSlot();
        $this->cutOffSlotsWithoutAnyFreeParticipant();
    }

    private function analyzeParticipantsCount()
    {
        var_dump($this->getPossibleParticipantsCount());
    }

    private function getPossibleParticipantsCount()
    {
        $tmp = array_unique($this->slotMatrix);
        $ret = array();

        foreach ($tmp as $participants) {
            $participantsCount         = substr_count(decbin($participants), '1');
            $ret[$participantsCount][] = $participants;
        }

        krsort($ret);

        return $ret;
    }

    private function transposeSlotMatrix()
    {
        if ($this->anyoneIsInvited()) {
            array_unshift($this->slotMatrix, null);

            $this->slotMatrix = call_user_func_array('array_map', $this->slotMatrix);
        }
    }

    /**
     * @return array
     */
    private function findMeetingStartsForAllInvited()
    {
        $slotsWithAllInvited = $this->getSlotsWithAllInvited();

        $ret  = array();
        $keys = array_keys($slotsWithAllInvited);
        foreach ($keys as $slot) {
            $ret[$slot] = (count(array_intersect($keys, range($slot, $slot + $this->numberOfSlotsRequiredForMeeting - 1))) == $this->numberOfSlotsRequiredForMeeting);
        }

        return array_intersect($ret, array(true));
    }

    private function anyoneIsInvited()
    {
        return (bool)$this->numberOfInvitedParticipants;
    }

    private function updateNumberOfParticipants()
    {
        $this->numberOfInvitedParticipants = count($this->invitedParticipants);
    }

    /**
     * @param $time
     * @return array
     */
    private function convertTimeToSlot($time, $endingSlot = false)
    {
        list($h, $m) = explode(':', $time);
        $minutes = $h * 60 + $m * 1;
        $slot    = $minutes / $this->getSlotDurationTime();
        $slot    = $endingSlot ? ceil($slot) : floor($slot);

        return intval($slot);
    }

    /**
     * @param $time
     * @return array
     */
    private function convertSlotToTime(&$time, $slot)
    {
        $minutes = $slot * $this->getSlotDurationTime();
        $time    = date('H:i', mktime(0, $minutes));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: krzysztofostrowski
 * Date: 27.03.2014
 * Time: 11:43
 */

namespace KO\Calendar\CalendarBundle\Logic;


class Participant {

    private $name;

    private $startOfWorkingHours;
    private $endOfWorkingHours;

    private $bookedSlots;

    /**
     * @param mixed $bookedSlots
     */
    public function setBookedSlots($bookedSlots)
    {
        $this->bookedSlots = $bookedSlots;
    }

    /**
     * @return mixed
     */
    public function getBookedSlots()
    {
        return $this->bookedSlots;
    }

    /**
     * @param mixed $endOfWorkingHours
     */
    public function setEndOfWorkingHours($endOfWorkingHours)
    {
        $this->endOfWorkingHours = $endOfWorkingHours;
    }

    /**
     * @return mixed
     */
    public function getEndOfWorkingHours()
    {
        return $this->endOfWorkingHours;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $startOfWorkingHours
     */
    public function setStartOfWorkingHours($startOfWorkingHours)
    {
        $this->startOfWorkingHours = $startOfWorkingHours;
    }
    /**
     * @return mixed
     */
    public function getStartOfWorkingHours()
    {
        return $this->startOfWorkingHours;
    }

    public function __construct($name = 'Participant')
    {
        $this->setName($name);
    }

} 